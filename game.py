"""Give a name and make comments"""
from initCache import initCache
from chooseGameBis import chooseGameBis
from displayCache import displayCache
from display import display
from evaluation import evaluation
from master import master


def game():
    nbC, nbP = 6, 4
    cache = [1, 5, 6, 4] #initCache(nbC, nbP)
    notFound = True
    tries = 1
    S = set((x, y, z, t) for x in range(1, 7) for y in range(1, 7) for z in range(1, 7) for t in range(1, 7))
    possible = frozenset(S)
    results = frozenset((well, bad) for well in range(5) for bad in range(5 - well) if not (well == 3 and bad == 1))
    while notFound and (tries <= 10):
        print('try', tries)
        selected = chooseGameBis(S, possible, results, tries)
        print('computer proposal: ', end='')

        displayCache(selected)
        print()
        well, bad = evaluation(selected, cache)
        display(well, bad)

        if well == nbP:
            notFound = False

        else:
            tries += 1
            S.difference_update(set(coup for coup in S if (well, bad) != evaluation(coup, selected)))

    if tries == 11:
        print("lost, we had to find:", end=' ')
        displayCache(cache)

    else:
        print("He is strong, he found", end=' ')
        displayCache(cache)


"""Give a name and make comments"""


def makeChoice():
    choice = input("1 for AI and 2 for Solo")
    if choice == "1":
        game()
    elif choice == "2":
        master()
    else:
        print("Goodbye")



