"""Check if the combination of the input pawns by the player is correct.
e.g : 2,2
"""


def evaluation(selected, cache):
    WellPut = 0
    Misplaced = 0
    copySelected, copyCache = list(selected), list(cache)

    for i in range(len(cache)):
        if copySelected[i] == copyCache[i]:
            WellPut += 1
            copySelected[i], copyCache[i] = -1, -1

    for i in range(len(cache)):
        for j in range(len(cache)):
            if (copySelected[i] == copyCache[j]) and (copySelected[i] != -1):
                Misplaced += 1
                copySelected[i], copyCache[j] = -1, -1

    return WellPut, Misplaced

