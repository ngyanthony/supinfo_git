"""Prompt user for a combination of pawns"""


def choose(nbColors=6, nbPawns=4):
    nocorrect = True

    while nocorrect:
        nocorrect = False
        selected = input('Input your proposal: ')  # Input range must be == nbPawns, in format e.g : 1,5,4,6
        if len(selected) == nbPawns:  # Input is what is expected
            selected = [int(x) for x in list(selected)] # Convert input to list

            for x in selected:
                if (x < 1) or (x > nbColors): # Check if input number is out of range of nbColors/invalid
                    nocorrect = True

        else:  # Repeat while loop
            nocorrect = True

    return selected


