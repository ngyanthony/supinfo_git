"""Give a name and make comments"""

from gameParameters import gameParameters
from initCache import initCache
from evaluation import evaluation
from choose import choose
from display import display
from displayCache import displayCache


def master():
    nbC, nbP, nbTry = gameParameters()
    cache = initCache(nbC, nbP)

    notFound = True

    tries = 1

    print()

    while notFound and (tries <= nbTry):
        print('try', tries)
        well, bad = evaluation(choose(nbC, nbP), cache)
        display(well, bad)

        if well == nbP:
            notFound = False

        else:
            tries += 1
        print("answer", displayCache(cache))

    if tries == nbTry + 1:
        print("lost, we had to find:", end=' ')
        displayCache(cache)

    else:
        print("Congratulations, you have found well:", end=' ')
        displayCache(cache)