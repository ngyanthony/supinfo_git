"""Give a name and make comments"""
from evaluation import evaluation


def chooseGameBis(S, possibles, results, tries):
    if tries == 1:
        return [1, 1, 2, 2]

    elif len(S) == 1:
        return S.pop()

    else:
        Max = 0
        for x in possibles:
            Min = 1297
            for res in results:
                nb = 0
                for p in S:
                    if evaluation(p, x) != res:
                        nb += 1

                if nb < Min:
                    Min = nb

            if Max < Min:
                Max = Min

                xx = x

        return xx