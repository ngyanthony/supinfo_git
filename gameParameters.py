"""Ask gameParameters."""


def gameParameters():
    nbC = int(input('Input the number of colors: '))
    nbP = int(input(' Enter the length of the sequence to guess: '))
    nbTry = int(input(' Enter the number of trials: '))

    return nbC, nbP, nbTry
