from random import randint

"""Return 1D array with nbPawns index and random nbColors value
e.g : [1, 6, 6, 4]
-> each index is based on nbPawns and each value is randomly choosed between 1 and 6
"""


def initCache(nbColors=6, nbPawns=4):
    return[randint(1, nbColors) for i in range(nbPawns)]

